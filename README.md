# irccd - Plugins

irccd: http://projects.malikania.fr/irccd/

## Install

```sh
sudo mkdir -p /usr/local/share/irccd/plugins
```

Place .js files in the above directory.

In irccd configuration file add at the bottom:

```sh
sudo nano /usr/local/etc/irccd/irccd.conf
```

```
[plugins]
tell = /usr/local/share/irccd/plugins/tell.js
rl = /usr/local/share/irccd/plugins/rl.js
download = /usr/local/share/irccd/plugins/download.js
```

