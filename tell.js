info = {
    author: "MangaD",
    license: "MIT",
    summary: "Leave a message to someone.",
    version: "1"
};

function onMessage(server, origin, channel, message) {

    var username = Irccd.Util.splituser(origin);

    sendMessages(server, channel, username);

}

function onCommand(server, origin, channel, message) {

    var username = Irccd.Util.splituser(origin);
    message = message.trim();

    if (!message || message === "") {
        server.message(channel, username + ": tell whom?");
        return;
    }

    var tokens = message.split(" ");

    if (tokens.length === 1) {
        server.message(channel, username + ": tell " + tokens[0] + " what?");
        return;
    } else if (tokens[0] === username) {
        server.message(channel, "You can tell yourself that.");
        return;
    } else if (!validNickname(tokens[0])) {
        server.message(channel, "Erroneous nickname: " + tokens[0]);
        return;
    }

    var res = writeMessageToFile(server, channel, username, tokens[0],
        message.substring(message.indexOf(" ")).trim());

    if (res) {
        server.message(channel, username + ": I'll pass that on when " + tokens[0] + " is around.");
    } else {
        server.message(channel, "Error writing to file.");
    }
}

function sendMessages(server, channel, destination) {

    var fp = new Irccd.File("/usr/local/share/irccd/plugins/tell.txt", "r");

    if (!fp) {
        return;
    }

    var data = fp.read();

    fp.close();

    if(!data || data === "") {
        return;
    }

    var se, ch, un, destination, message, timestamp;
    var tokens = data.split("\n");
    var deleted = [];

    var i = 0;
    var t;
    var self = this;
    t = new Irccd.Timer(Irccd.Timer.Repeat, 1000, function () { // 1 second

        if (i >= tokens.length-1) {
            deleteEntries();
            self.timer.stop();
            return;
        }

        if (i + 5 >= tokens.length-1) {
            server.message(channel, "tell.txt is malformed.");
            self.timer.stop();
            return;
        }

        se = tokens[i];
        ch = tokens[++i];
        un = tokens[++i];
        dest = tokens[++i];
        message = tokens[++i];
        timestamp = tokens[++i];

        if (se === server.toString() && ch === channel.toString() && dest === destination.toString()) {

            deleted.push(i);

            server.message(channel, dest + ": <" + un + "> " + message + " (" + timestamp + ")");
        }

        ++i;
    });

    this.timer = t;
    t.start();

    function deleteEntries() {
        if (deleted.length > 0) {

            fp = new Irccd.File("/usr/local/share/irccd/plugins/tell.txt", "w");

            for (i = 0; i < tokens.length-1; ++i) {
                var del = false;
                var j;
                for (j = 0; j < deleted.length; ++j) {
                    if (deleted[j] - i <= 5) {
                        del = true;
                    }
                }
                if (!del) {
                    fp.write(tokens[i] + "\n");
                }
            }

            fp.close();
        }
    }
}

function writeMessageToFile(server, channel, username, destination, message) {

    var fp = new Irccd.File("/usr/local/share/irccd/plugins/tell.txt", "a");

    if (!fp) {
        return false;
    }

    fp.write(server + "\n");
    fp.write(channel + "\n");
    fp.write(username + "\n");
    fp.write(destination + "\n");
    fp.write(message + "\n");

    var m = new Date();
    var dateString =
        m.getUTCFullYear() + "-" +
        ("0" + (m.getUTCMonth()+1)).slice(-2) + "-" +
        ("0" + m.getUTCDate()).slice(-2) + " - " +
        ("0" + m.getUTCHours()).slice(-2) + ":" +
        ("0" + m.getUTCMinutes()).slice(-2) + ":" +
        ("0" + m.getUTCSeconds()).slice(-2);

    fp.write(dateString + "\n");
    fp.close();

    return true;

}

function validNickname(nick) {
    var regex = /^[a-zA-Z0-9.\[\]}{\\\`\|_-]*$/;
    if (nick.length > 9 || !regex.test(nick)) {
        return false;
    }
    return true;
}
