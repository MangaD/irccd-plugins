info = {
    author: "MangaD",
    license: "MIT",
    summary: "HF download links.",
    version: "1"
};

function onCommand(server, origin, channel, message) {
    var links = {
        "HFv0.7+ w/ RS": "https://www.mediafire.com/file/8yoe65k7o1me8cw/HFv0.7_mod.zip/file",
        "Room Server": "https://hf-empire.com/downloads/servers/RS_0.7_1.0a_MangaD.zip",
        "HF Workshop": "https://hf-empire.com/downloads/hf-workshop/HFWorkshop_x86.exe"
    };

    var keysArray = Object.keys(links);
    var valuesArray = Object.keys(links).map(function(k) {
        return String(links[k]);
    });

    var i = 0;
    var t;
    var self = this;
    t = new Irccd.Timer(Irccd.Timer.Repeat, 1000, function () { // 1 second
        if (i >= valuesArray.length) {
            self.timer.stop();
            return;
        }

        server.message(channel, "\x02" + keysArray[i] + "\x0f" + ": " + valuesArray[i]);

        ++i;
    });
    this.timer = t;
    t.start();
}

